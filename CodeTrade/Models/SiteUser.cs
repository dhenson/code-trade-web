﻿using System;
using System.Collections.Generic;


namespace CodeTrade.Models
{
    public partial class SiteUser
    {
        public int SiteUserID { get; set; }
        public string SiteUserFirstName { get; set; }
        public string SiteUserLastName { get; set; }
        public string SiteUserEmail { get; set; }
        public string SiteUserPassword { get; set; }


        public SiteUser()
        {
            
        }


    }


}