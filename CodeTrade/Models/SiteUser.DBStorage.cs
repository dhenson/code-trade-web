﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;

using CodeTrade.Helpers;

namespace CodeTrade.Models
{
    public partial class SiteUser
    {
        static public SiteUser GetUser(int id)
        {
            string sql = @"
SELECT * 
FROM siteusers 
WHERE siteuserid = @SiteUserID";

            SqlParameter[] sqlparameters = new SqlParameter[1];
            sqlparameters[1] = new SqlParameter("SiteUserID", id);

            SqlDataReader dr = DBUtil.GetReader(sql, sqlparameters);


            SiteUser s = new SiteUser();

            if (dr.Read())
            {
                s.SiteUserID = Convert.ToInt32(dr["SiteUserID"]);
                s.SiteUserFirstName = dr["SiteUserFirstName"].ToString();
                s.SiteUserLastName = dr["SiteUserLastName"].ToString();
                s.SiteUserEmail = dr["SiteUserEmail"].ToString();
                s.SiteUserPassword = dr["SiteUserPassword"].ToString();
            }

            dr.Close();

            return s;
        }



        static public List<SiteUser> GetAllUsers()
        {
            List<SiteUser> siteusers = new List<SiteUser>();

            //query list of users...
            string sql = @"
SELECT * 
FROM SiteUsers";

            SqlDataReader dr = DBUtil.GetReader(sql);

            while (dr.Read())
            {
                SiteUser s = new SiteUser();

                s.SiteUserID = Convert.ToInt32(dr["SiteUserID"]);
                s.SiteUserFirstName = dr["SiteUserFirstName"].ToString();
                s.SiteUserLastName = dr["SiteUserLastName"].ToString();
                s.SiteUserEmail = dr["SiteUserEmail"].ToString();
                s.SiteUserPassword = dr["SiteUserPassword"].ToString();

                siteusers.Add(s);
            }

            dr.Close();


            return siteusers;
        }

        static public int Delete(int siteuserid)
        {
            string sql = "delete from SiteUsers where siteuserid = " + siteuserid.ToString();

            DBUtil.Exec(sql);

            return siteuserid;
        }
    }
}