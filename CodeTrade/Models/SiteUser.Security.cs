﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;

using CodeTrade.Helpers;

namespace CodeTrade.Models
{
    public partial class SiteUser
    {
        public SiteUser(string email)
        {
            string sql = @"
SELECT 
  SiteUserID,
  SiteUserFirstName,
  SiteUserLastName,
  SiteUserEmail,
  SiteUserPassword
FROM siteusers
WHERE 
    SiteUserEmail = @emailtotry

";
            SqlParameter[] queryparms = new SqlParameter[1];
            queryparms[0] = new SqlParameter("@emailtotry", email);

            SqlDataReader dr = DBUtil.GetReader(sql, queryparms);


            if(dr.Read())
            {
                this.SiteUserID = Convert.ToInt32( dr["SiteUserID"]);
                this.SiteUserFirstName = dr["SiteUserFirstName"].ToString();
                this.SiteUserLastName = dr["SiteUserLastName"].ToString();
                this.SiteUserEmail = dr["SiteUserEmail"].ToString();
                this.SiteUserPassword = dr["SiteUserPassword"].ToString();
            }
            dr.Close();
        }
    }
}