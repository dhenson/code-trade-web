﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using CodeTrade.Helpers;

using System.Data.SqlClient;

namespace CodeTrade
{
    public class MvcApplication : System.Web.HttpApplication
    {

        protected void Session_Start()
        {
            string sql = @"
Insert WebLogs(WebLogDate, WebLogPage, WebLogSessionID) Values(@date, @page, @session)";

            SqlParameter[] queryparms = new SqlParameter[3];
            queryparms[0] = new SqlParameter("date", DateTime.Now);
            //queryparms[1] = new SqlParameter("page", )

            //DBUtil.Exec(sql,

//Insert WebLogs(WebLogDate, WebLogPage, WebLogSessionID) 
            //Values(getdate(), 'Products', '1234356')
        }

        protected void Application_Start()
        {

            Application["MySiteName"] = @"cool site";

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
