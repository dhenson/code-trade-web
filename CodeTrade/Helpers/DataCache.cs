﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;

using CodeTrade.Models;

namespace CodeTrade.Helpers
{
    public class DataCache
    {
        List<SiteUser> siteusers;
        DateTime expirationDate;

        public List<SiteUser> storedList()
        {
            DateTime requestTime = new DateTime();
            if  (requestTime > expirationDate)
            {
                //hit the database and refresh the list...
                expirationDate = DateTime.Now.AddMinutes(1);
            }          
            else
            {
                //do nothing...
            }

            return siteusers;
        }
    }
}