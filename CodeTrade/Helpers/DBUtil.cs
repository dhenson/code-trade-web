﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace CodeTrade.Helpers
{
    public class DBUtil
    {
        static public int Exec(string sql)
        {

            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Config.ConnectionString;

                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                //log error

                //email dave

                //reboot box

                throw new Exception(@"Error from DBUtil", ex.InnerException);
            }
            finally
            {
                //conn.Close();
            }

            return 0;



        }
        static public int Exec(string sql, SqlParameter[] queryparms)
        {

            SqlConnection conn = new SqlConnection();

            try
            {
                conn.ConnectionString = Config.ConnectionString;

                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);
                foreach(SqlParameter p in queryparms)
                {
                    cmd.Parameters.Add(p);
                }

                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                //log error

                //email dave

                //reboot box

                throw new Exception(@"Error from DBUtil", ex.InnerException);
            }
            finally
            {
                //conn.Close();
            }

            return 0;



        }

        static public SqlDataReader GetReader(string sql)
        {
            SqlConnection conn = new SqlConnection();
            SqlDataReader dr;

            try
            {
                conn.ConnectionString = Config.ConnectionString;

                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception ex)
            {
                //log error

                //email dave

                //reboot box

                throw new Exception(@"Error from DBUtil", ex.InnerException);
            }
            finally
            {
                //conn.Close();
            }

            return dr;


        }

        static public SqlDataReader GetReader(string sql, SqlParameter[] sqlparms)
        {
            SqlConnection conn = new SqlConnection();
            SqlDataReader dr;

            try
            {
                conn.ConnectionString = Config.ConnectionString;

                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);
                foreach(SqlParameter p in sqlparms)
                {
                    cmd.Parameters.Add(p);
                }

                dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            }
            catch (Exception ex)
            {
                //log error

                //email dave

                //reboot box

                throw new Exception(@"Error from DBUtil", ex.InnerException);
            }
            finally
            {
                //conn.Close();
            }

            return dr;


        }

    }
}