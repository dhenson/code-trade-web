﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;

using CodeTrade.Helpers;

namespace CodeTrade.Controllers
{
    public class PerformanceController : Controller
    {
        // GET: Performance
        public ActionResult Index()
        {
            string sql = @"
select * 
from big
where bigdate='2016-10-15 11:32:07.097'

";
            SqlDataReader dr = DBUtil.GetReader(sql);

            dr.Read();

            DateTime rowDate = Convert.ToDateTime(dr["BigDate"]);

            ViewBag.rowDate = rowDate;

            dr.Close();

            return View();
        }
    }
}