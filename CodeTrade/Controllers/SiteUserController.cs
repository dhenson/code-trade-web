﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Data;
using System.Data.SqlClient;

using CodeTrade.Helpers;
using CodeTrade.Models;




namespace CodeTrade.Controllers
{
    public class SiteUserController : Controller
    {
        // GET: SiteUser
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SetCookie()
        {
//            Request.Cookies[""]
            




            return View();
        }

        public ActionResult ReadCookie()
        {
            ViewBag.UserID = Session["UserID"];

            return View();
        }

        [HttpGet]
        public ActionResult SetSession()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SetSession(string id)
        {
            Session["UserID"] = id;

            Response.Redirect("ReadSession");

            return View();
        }


        public ActionResult ReadSession()
        {
            ViewBag.UserID = Session["UserID"];

            return View();
        }

        public ActionResult ReadApplication()
        {
            ViewBag.MySiteName = HttpContext.Application["MySiteName"].ToString();

            return View();
        }

        [HttpGet]
        public ActionResult SiteUserLookup()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SiteUserLookup(string id)
        {
            SiteUser s = new SiteUser(id);

            if(s != null)
            {
                ViewBag.SiteUser = s;
            }

            return View();
        }





        public ActionResult AllUsers()
        {
            List<SiteUser> siteusers = SiteUser.GetAllUsers();

            ViewBag.siteusers = siteusers;

            return View();
        }

        public ActionResult UserDetail(string id)
        {
            //check all parameters first...

            SiteUser s = new SiteUser();

            string sql = "select * from siteusers where siteuserid=@siteuserid";

            SqlParameter[] myparms = new SqlParameter[1];
            myparms[0] = new SqlParameter("siteuserid", id);

            SqlDataReader dr = DBUtil.GetReader(sql, myparms);


            dr.Read();
            
            s.SiteUserEmail = dr["SiteUserEmail"].ToString();

            ViewBag.siteuser = s;

            dr.Close();

            return View(s);
        }

        [HttpGet]
        public ActionResult Login()
        {
            ViewBag.LoginMessage = @"Please login...";

            return View();
        }


        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            //parameter checking...

            ViewBag.email = email;

            SiteUser s = new Models.SiteUser(email);

            if(s.SiteUserEmail != null)
            {
                //check password...
                if(s.SiteUserPassword != password)
                {
                    ViewBag.LoginMessage = @"Wrong Password";
                }
                else
                {
                    //login successful...
                    Session["SiteUser"] = s;
                    ViewBag.LoginMessage = @"Welcome " + s.SiteUserFirstName;
                }

            //if password matches...redirect
            }
            else
            {
                ViewBag.LoginMessage = @"Email Not Found.";
            }


            return View();
        }


    }
}