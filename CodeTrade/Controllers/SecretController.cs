﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using CodeTrade.Helpers;
using CodeTrade.Models;

namespace CodeTrade.Controllers
{
    public class SecretController : Controller
    {
        // GET: Secret
        public ActionResult Index()
        {

            if(Session["SiteUser"] == null)
            {
                Response.Redirect("~/SiteUser/Login");
            }

            return View();
        }
    }
}