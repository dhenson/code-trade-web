﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using CodeTrade.Models;

namespace CodeTrade.Controllers
{
    public class SiteUsersController : ApiController
    {
        // GET api/<controller>
        public List<SiteUser> Get()
        {
            return SiteUser.GetAllUsers();
        }

        // GET api/<controller>/5
        public SiteUser Get(int id)
        {
            SiteUser s = SiteUser.GetUser(id);

            return s;
        }



        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            //tons of parameter checking...

            SiteUser.Delete(id);
            
        }
    }
}