
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    csssplit: {
        IEcss: {
            src: ['Content/CSS/main.css'],
            dest: 'Content/CSS/main.min.css',
            options: {
                maxSelectors: 4095,
                maxPages: 3,
                suffix: '_'
            }
        },
    },

    // Grunt-sass 
    sass: {
        dist: {
            files: [
                {
                    expand: true,     // Enable dynamic expansion.
                    cwd: 'Sass/',      // Src matches are relative to this path.
                    src: ['*.scss'], // Actual pattern(s) to match.
                    dest: 'Content/CSS/',   // Destination path prefix.
                    ext: '.css',   // Dest filepaths will have this extension.
                },
            ],
        },
        options: {
            sourceMap: true,
            outputStyle: 'compressed',
            require: 'susy',
            includePaths: ['node_modules']
        }
    },
    uglify: {
        options: {
            //mangle: false
        },
        js: {
            files: [{
                expand: true,
                cwd: 'Scripts/',
                src: '**/*.js',
                dest: 'Content/Scripts/'
            }]
        }
    },

    //All task that are under watch
    watch: {
        sass: {
            files: ['sass/**/*.{scss,sass}'],
            tasks: ['doCSS'],
            options: { event: ['added', 'changed'] },
        },
        js: {
            files: ['Scripts/**/*.js'],
            tasks: ['uglify'],
            options: { event: ['added', 'changed'] },
        },

        
        options: {
            spawn: false,
        }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks("grunt-csssplit");
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Compiles Sass into CSS and make the IE split
  grunt.registerTask('doCSS', ['sass', 'csssplit']);


};